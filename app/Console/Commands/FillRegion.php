<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Csv\Reader;
use App\Model\Region;

class FillRegion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regions:fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill region';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = storage_path('files/regions_201911211037.csv');
        $csv = Reader::createFromPath($path, 'r');

        $csv->setHeaderOffset(0);

        $header = $csv->getHeader();
        $records = $csv->getRecords();
        // dd($records);

        foreach($records as $record) {

            $id = (int) $record['id'];
            $parentId = (int) $record['parent_id'];

            if($parentId == 0) {$parentId = NULL;}

            $level = (int) $record['level'];
            $left = (int) $record['left'];
            $right = (int) $record['right'];
            $timezone = (int) $record['timezone'];

            $postalCode = ($record['postal_code'] == '') ? NULL: $record['postal_code'];

            Region::create([
                'id'    => $id,
                'name'  => $record['name'],
                '_lft'  => $left,
                '_rgt'  => $right,
                'parent_id' => $parentId,
                'level' => $level + 1,
                'timezone' => $timezone
            ]);

            $this->info('Created ' . $record['name']);
        }
    }
}
