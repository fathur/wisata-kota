<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Agent;

class AgentController extends Controller
{
    public function index()
    {
        $user = \Auth::user();

        return view('agents.index')->with([
            'user'  => $user,
            'trips' => optional($user->agent)->trips
        ]);
    }

    public function create()
    {
        return view('agents.create');
    }

    public function store(Request $request)
    {
        $user = \Auth::user();

        $agent = $user->agent()->create([
            'region_id' => $request->get('district'),
            'name'  => $request->get('name'),
            'address' => $request->get('address')
        ]);


        if($agent) {
            return redirect()->to('agents/me');
        }
    }
}
