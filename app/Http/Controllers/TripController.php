<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Trip;
use Carbon\Carbon;

class TripController extends Controller
{
    public function index()
    {
        $trips = Trip::all();

        return view('trips.index', compact('trips'));
    }

    public function view(Trip $trip)
    {        
        return view('trips.view', compact('trip')); //->with(['trip' => $tripId]);

    }

    public function booking(Trip $trip, Request $request)
    {
        // dd($trip);
        // $trip = Trip::find($trip);
        $past = (bool) $request->get('past', false);
        return view('trips.booking')->with([
            'past'  => $past,
            'trip'  => $trip
        ]);
    }

    /**
     * Only accessible for agen trip
     */
    public function agentCreate()
    {
        return view('trips.agent-create');
    }

    public function agentView(Trip $trip)
    {
        return view('trips.agent-view', compact('trip'));

    }

    public function agentStore(Request $request)
    {
        $user = \Auth::user();

        $trip = $user->agent->trips()->create([
            "name" => $request->get('name'),
            "description" => $request->get('description'),
            "price" => (int) $request->get('price'),
            "start_at" => Carbon::parse($request->get('start_at')),
            "end_at" => Carbon::parse($request->get('end_at')),
            "quota" => (int) $request->get('quota'),
            "region_id" => (int) $request->get('district')
        ]);

        return redirect()->route('agents.trips.view', $trip->id);
    }
}
