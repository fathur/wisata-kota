<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Trip;

class DashboardController extends Controller
{
    public function index()
    {
        $trips = Trip::orderBy('created_at')->take(4)->get();

        return view('dashboard.index')->with([
            'trips' => $trips
        ]);
    }
}
