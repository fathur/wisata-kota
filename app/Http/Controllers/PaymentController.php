<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function method($bookingId)
    {
        return view('payments.method');
    }

    public function detail($bookingId)
    {
        return view('payments.detail');
    }

    public function status($bookingId)
    {
        return view('payments.status');
    }
}
