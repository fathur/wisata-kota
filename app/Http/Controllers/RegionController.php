<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Region;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\RegionTransformer;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $level = $request->get('level', 1);
        $parent = $request->get('parent');
        $searchText = strtolower($request->get('q'));

        // dd($searchText);

        if(is_null($parent)) {
            $regions = Region::where('level', 1)->orderBy('name');
        } else {
            $regions = Region::where('parent_id', (int) $parent)->orderBy('name');
        }

        if(is_null($searchText)) {
            $regions = $regions->get();
        } else {
            $regions = $regions->whereRaw("LOWER(name) like '%{$searchText}%'")->get();
        }

        $resource = new Collection($regions, new RegionTransformer);
        $fractal = new Manager();
        $array = $fractal->createData($resource)->toArray();
        return response()->json($array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
