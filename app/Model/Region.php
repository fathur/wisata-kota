<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Region extends Model
{
    use NodeTrait;

    protected $fillable = ['id', 'name', '_lft', '_rgt', 'parent_id', 'timezone', 'level'];
}
