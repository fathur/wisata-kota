<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Region;

class Trip extends Model
{
    protected $fillable = ['name', 'price', 'quota', 'region_id', 'start_at', 'end_at', 'description', 'agent_id'];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }
}
