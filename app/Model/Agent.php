<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trip;

class Agent extends Model
{
    protected $fillable = ['region_id', 'address', 'name'];

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }
}
