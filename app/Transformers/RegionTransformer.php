<?php

namespace App\Transformers;

use League\Fractal;
use App\Model\Region;

class RegionTransformer extends Fractal\TransformerAbstract
{
    public function transform(Region $region)
	{
	    return [
	        'id'      => (int) $region->id,
	        'name'   => $region->name,
	        
	    ];
	}
}
