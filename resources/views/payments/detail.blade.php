@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card mb-2">
                        <div class="card-body text-center text-white bg-danger">
                            <h2 class="mb-0 mt-1">Rp.3.900.000</h2>
                        </div>
                    </div>

                    <div class="card mb-2">
                        <div class="card-header bg-transparent">Transfer BCA</div>
                        <div class="card-body">
                            <ol>
                                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta, tempora aliquam ratione, sed dolor fugit doloribus non eveniet id iure optio? Eveniet in natus commodi accusantium doloremque saepe recusandae pariatur.</li>
                                <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellendus a excepturi adipisci, porro ad sed rerum aspernatur suscipit provident alias nulla possimus facilis quo nihil laborum, dolor nemo ipsa non.</li>
                                <li>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptates laudantium cupiditate mollitia illum, molestias, eveniet alias atque perspiciatis praesentium maiores quam doloremque nihil aut harum quibusdam unde eum ratione dolores.</li>
                                <li>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Magni quia illo cum enim consequatur minus dignissimos distinctio, amet nihil laborum voluptate delectus hic harum quasi, sapiente rerum recusandae ipsa omnis?</li>
                            </ol>
                        </div>
                    </div>

                    <div class="text-center mb-2">
                        <a href="{{route('payments.method', 3)}}">Pilih metode pembayaran lain</a>
                        <div class="clearfix mb-1"></div>
                        <a href="{{route('payments.status', 3)}}" class="btn btn-lg btn-danger">Bayar</a>
                    </div>

                </div>
                
            </div>
        </div>
    </div>

    
@endsection