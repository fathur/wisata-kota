@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h2>Metode Pembayaran</h2>
                        <div class="accordion mb-3" id="paymentMethodAccordion">
                            <div class="card border-danger">
                                <div class="card-header border-danger" id="virtualAccountHeading">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#virtualAccountCollapse" aria-expanded="true" aria-controls="virtualAccountCollapse">
                                        Virtual Account
                                        </button>
                                    </h2>
                                </div>
                              
                                <div id="virtualAccountCollapse" class="collapse show" aria-labelledby="virtualAccountHeading" data-parent="#paymentMethodAccordion">
                                    <ul class="list-group">
                                        <li class="list-group-item"><a href="#">BCA</a></li>
                                        <li class="list-group-item"><a href="#">Mandiri</a></li>
                                    </ul>
                                
                                </div>
                            </div>
                            
                            <div class="card border-danger">
                                    <div class="card-header border-danger" id="transferHeading">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#transferCollapse" aria-controls="virtualAccountCollapse">
                                            Transfer
                                            </button>
                                        </h2>
                                    </div>
                                  
                                    <div id="transferCollapse" class="collapse" aria-labelledby="transferHeading" data-parent="#paymentMethodAccordion">
                                        <ul class="list-group">
                                            <li class="list-group-item"><a href="{{route('payments.detail', 3)}}">BCA</a></li>
                                            <li class="list-group-item"><a href="#">Mandiri</a></li>
                                        </ul>
                                    
                                    </div>
                                </div>
                        </div>
                </div>
                
            </div>
        </div>
    </div>

    
@endsection