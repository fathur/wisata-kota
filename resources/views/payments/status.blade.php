@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card mb-2">
                        <div class="card-body text-center">
                            <p>Lakukan pembayaran dalam</p>
                            <h2 class="mb-0 mt-1">22 jam : 30 menit : 10 detik</h2>
                        </div>
                    </div>

                    <div class="card mb-2">
                        <div class="card-body text-center">
                            <h4>Ke Nomor: 2393-9384-1122-3939-9393</h4>
                            <h4>Sejumlah: Rp3.900.000</h4>
                        </div>
                    </div>

                    <div class="text-center mb-2">
                        <a href="{{route('payments.status', 3)}}" class="btn btn-success"><i class="fal fa-sync"></i> Cek status pembayaran</a>
                    </div>

                </div>
                
            </div>
        </div>
    </div>

    
@endsection