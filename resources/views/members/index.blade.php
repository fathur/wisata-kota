@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8">
                   <div class="card mb-2">
                       <div class="card-header">
                           Peserta
                       </div>

                       <table class="table">
                           <thead>
                               <tr>
                                   <th>Id</th>
                                   <th>Nama</th>
                                   <th>KTP</th>
                                   <th>Phone</th>
                                   <th>Email</th>
                                   <th>Pembayaran</th>
                               </tr>
                           </thead>

                           <tbody>
                               <tr>
                                   <td>#9393</td>
                                   <td>Abdul</td>
                                   <td>07r8923045</td>
                                   <td>+62 8383838383</td>
                                   <td>jdsjdsj@yahoo.com</td>
                                   <td>Lunas</td>
                               </tr>

                               <tr>
                                    <td>#9393</td>
                                    <td>Abdul</td>
                                    <td>07r8923045</td>
                                    <td>+62 8383838383</td>
                                    <td>jdsjdsj@yahoo.com</td>
                                    <td>Lunas</td>
                                </tr>
                           </tbody>
                       </table>

                       
                   </div>

                   
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h4>Gunung Merbabu</h4>
                            <p>27 - 30 Agustus 2019</p>
                            <p>30/40</p>
                            <p><h5>Rp30.000.000</h5></p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    
@endsection