@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            @if($user->isNotAgent())
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card mb-2">
                        <div class="card-body text-center ">
                            <p>Anda belum terdaftar menjadi Agen wisata. <br/>Silahkan klik tombol dibawah untuk mendaftarkan diri menjadi agen.</p>
                            <a href="{{route('agents.create')}}" class="btn btn-primary btn-lg">Daftar menjadi agen wisata</a>
                        </div>
                    </div>

                    
                </div>
                
            </div>
            @else

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-2">
                        <div class="card-header">
                            Wisata saya

                        <a href="{{route('agents.trips.create')}}" class="btn float-right btn-sm btn-success">Tambah wisata</a>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Lokasi</th>
                                    <th>Tanggal</th>
                                    <th>Kuota</th>
                                    <th>Status</th>
                                    {{-- <th>&nbsp;</th> --}}
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($trips as $trip)
                                    @if(!is_null($trip))
                                    
                                <tr>
                                <td><a href="{{route('agents.trips.view', $trip->id)}}">#{{$trip->id}}</a></td>
                                    <td>{{optional($trip)->name}}</td>
                                    <td>{{optional($trip)->price}}</td>
                                    <td>{{optional($trip)->region->name}}</td>
                                    <td>{{optional($trip)->start_at}} - {{optional($trip)->end_at}}</td>
                                    <td>{{optional($trip)->quota}}</td>
                                    <td>Berjalan</td>
                                    {{-- <td>
                                        <a href="#" class="btn btn-sm btn-warning">Edit</a>
                                    </td> --}}
                                </tr>
                                    @endif
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

    
@endsection