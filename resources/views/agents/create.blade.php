@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card mb-2">
                        <div class="card-body">
                            <h4>Buat agen wisata</h4>
                            <p>Satu akun hanya bisa memiliki satu agen wisata</p>
                            <form action="{{route('agents.store')}}" method="POST">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="name">Nama agen</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>

                                <div class="form-group">
                                    <label for="province">Provinsi</label>
                                    <select name="province" id="province"></select>
                                </div>

                                <div class="form-group">
                                    <label for="city">Kabupaten</label>
                                    <select name="city" id="city"></select>

                                </div>

                                <div class="form-group">
                                    <label for="district">Kecamatan</label>
                                    <select name="district" id="district"></select>

                                </div>

                                <div class="form-group">
                                    <label for="address">Alamat</label>
                                    <textarea name="address" id="address" class="form-control"></textarea>
                                </div>

                                <button type="submit" class="btn btn-primary float-right">Simpan</button>
                            </form>
                        </div>
                    </div>

                    
                </div>
                
            </div>
        </div>
    </div>

    
@endsection

@push('script')
<script>
function formatRepoSelection (repo) {
    console.log(repo);
  return repo.name || repo.text;
}

function formatRepo (repo) {
    console.log(repo);

  if (repo.loading) {
    return repo.text;
  }

  return repo.name;

}

$('#province').select2({
  ajax: {
    url: "{{route('regions.index')}}",
    dataType: 'json',

    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    delay: 250,
    data: function (params) {
      return {
        q: params.term, // search term
        // page: params.page
      };
    },
    processResults: function (data, params) {
        // console.log(data);
        return {
            results: $.map(data.data, function (item) {
                return {
                    text: item.name,
                    id: item.id
                }
            })
        }
    },
  },
  placeholder: 'Cari provinsi',
  minimumInputLength: 0,
  width: '100%',
//   templateResult: formatRepo,
//   templateSelection: formatRepoSelection
});

$('#city').select2({
    width: '100%',
    placeholder: 'Cari Kabupaten',
});

$('#district').select2({
    width: '100%',
    placeholder: 'Cari Kecamatan',
});

$('#province').on('select2:select', function (e) {
    
    var data = e.params.data;
    console.log(data);

    $('#city').select2({
        ajax: {
            url: "{{route('regions.index')}}?parent="+data.id,
            dataType: 'json',

            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            delay: 250,
            data: function (params) {
            return {
                q: params.term, // search term
                // page: params.page
            };
            },
            processResults: function (data, params) {
                return {
                    results: $.map(data.data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                }
            },
        },
        placeholder: 'Cari Kabupaten',
        minimumInputLength: 0,
        width: '100%',
        //   templateResult: formatRepo,
        //   templateSelection: formatRepoSelection
    });
});

$('#city').on('select2:select', function (e) {
    
    var data = e.params.data;

    $('#district').select2({
        ajax: {
            url: "{{route('regions.index')}}?parent="+data.id,
            dataType: 'json',

            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            delay: 250,
            data: function (params) {
            return {
                q: params.term, // search term
                // page: params.page
            };
            },
            processResults: function (data, params) {
                return {
                    results: $.map(data.data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                }
            },
        },
        placeholder: 'Cari Kecamatan',
        minimumInputLength: 0,
        width: '100%',
        //   templateResult: formatRepo,
        //   templateSelection: formatRepoSelection
    });
});
</script>
@endpush