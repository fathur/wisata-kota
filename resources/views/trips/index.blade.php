@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-3">
                    
                    <div class="card mt-2 mb-2">
                        <div class="card-header">
                            Harga
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <input type="text" class="form-control form-control" placeholder="Harga minimum">
                            </div>

                            <div class="form-group">
                               <input type="text" class="form-control form-control" placeholder="Harga maksimum">
                            </div>
                        </div>
                    </div>

                    <div class="card mt-2 mb-2">
                        <div class="card-header">
                            Kategori
                        </div>
                        <div class="card-body">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="a">
                                <label class="form-check-label" for="a">
                                    Default checkbox
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="b">
                                <label class="form-check-label" for="b">
                                    Default checkbox
                                </label>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-block btn-primary">Filter</button>

                 
                </div>

                <div class="col-md-9">
                    <div class="row">
                        @foreach ($trips as $trip)
                            
                        <div class="col-md-3">
                            <div class="list-trip-item mt-2 mb-2">
                
                                <img src="https://picsum.photos/400" alt="{{$trip->name}}" class="img-fluid">
                
                                <div class="description">
                
                                    <a href="{{route('trips.view', $trip->id)}}"><h3>{{$trip->name}}</h3></a>
                                    <p>{{$trip->price}}</p>
                                </div>
                
                            </div>
                        </div>
                        @endforeach           
                        
                    </div>
                        
                </div>
            </div>
        </div>
    </div>

    
@endsection