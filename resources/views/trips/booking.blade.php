@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8">
                    

                    <div class="card mb-2 text-white bg-success">
                        {{-- <div class="card-header">
                            Gunung Merbabu
                        </div> --}}
                        <div class="card-body">
                            <h3>{{$trip->name}}</h3>
                            <ol class="list-unstyled">
                                {{-- <li>
                                    <i class="fad fa-fw fa-sack"></i> 3 x @Rp.300.000 = <b style="font-size: 30px;" class="text-danger">Rp900.000</b>
                                </li> --}}
                                <li>
                                        <i class="fad fa-fw fa-map-marker-alt"></i> Dieng, Jawa Tengah
                                    </li>
                                <li>
                                    <i class="fad fa-fw fa-calendar-alt"></i> {{$trip->start_at}} - {{$trip->end_at}}
                                </li>
                            </ol>
                        </div>
                    </div>

                    @if($past)
                    <div class="card mb-2 border-success">
                        <div class="card-header border-success bg-success text-white">
                            Ulasan
                        </div>
                        <div class="card-body border-success">
                            <p>Ceritakan pengalaman wisata Anda disini</p>
                            <textarea name="review" id="review" class="form-control" placeholder="Ulasan"></textarea>
                            <button class="btn btn-primary mt-1 float-right">Simpan</button>
                        </div>
                    </div>

                    <div class="card mb-2 border-success">
                            <div class="card-header bg-success text-white border-success">
                                Feedback
                            </div>
                            <div class="card-body border-success">
                                <p>Menurut Anda, bagaimana kualitas agen wisata Agen Resmi Terpercaya dalam melayani Anda?</p>
                                <textarea name="review" id="review" class="form-control" placeholder="Ulasan"></textarea>
                                <button class="btn btn-primary mt-1 float-right">Simpan</button>

                            </div>
                        </div>
                    @endif

                    <div class="card mb-2 border-success">
                        <div class="card-header border-success">
                            <div class="float-left"><h5 class=" mt-1 mb-0"><i class="fal fa-users"></i> Peserta</h5></div>
                            @unless ($past)
                            <a href="#" class="btn float-right btn-success btn-sm">Tambah peserta</a>
                                
                            @endunless
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>KTP</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for ($i = 0; $i < 3; $i++)
                                    <tr>
                                        <td>{{$i+1}}</td>
                                        <td>Aceng</td>
                                        <td>4543534</td>
                                        <td>+62 454534</td>
                                        <td>dsfsd@gmail.com</td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                        <div class="card-footer border-success">
                            <i class="fad fa-fw fa-sack"></i> 3 x @Rp.300.000 = <b style="" class="text-danger float-right">Rp900.000</b>
                        </div>
                    </div>

                    <div class="card mb-2 border-primary">
                        <div class="card-header border-primary">
                                <div class="float-left"><h5 class=" mt-1 mb-0"><i class="fal fa-car-bus"></i> Transportasi</h5></div>
                                @unless ($past)

                                <a href="#" class="btn float-right btn-primary btn-sm">Tambah transportasi</a>
                                @endunless
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Maskapai</th>
                                    <th>Asal</th>
                                    <th>Tujuan</th>
                                    <th>Jumlah</th>
                                    <th>Harga</th>
                                    <th>Total</th>
                                    @unless ($past)
                                    <th>&nbsp;</th>
                                        
                                    @endunless
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @for ($i = 0; $i < 3; $i++) --}}
                                    <tr>
                                        <td>1</td>
                                        <td>Garuda</td>
                                        <td>Jakarta</td>
                                        <td>Semarang</td>
                                        <td>3</td>
                                        <td>Rp500.000</td>
                                        <td>Rp1.500.000</td>
                                        @unless ($past)
                                        <td>
                                            <a href="#" class="btn btn-sm btn-warning">Edit</a>
                                        </td>
                                        @endunless
                                       
                                    </tr>
                                {{-- @endfor --}}
                            </tbody>
                        </table>
                        <div class="card-footer border-primary">
                            <i class="fad fa-fw fa-sack"></i> <b style="" class="text-danger float-right">Rp1.500.000</b>
                            
                        </div>
                    </div>

                    <div class="card mb-2 border-secondary">
                        <div class="card-header border-secondary">
                                <div class="float-left"><h5 class=" mt-1 mb-0"><i class="fal fa-bed"></i> Penginapan</h5></div>
                                @unless ($past)
                                <a href="#" class="btn float-right btn-secondary btn-sm">Tambah penginapan</a>
                                    
                                @endunless
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Lokasi</th>
                                    
                                    <th>Total</th>
                                    @unless ($past)
                                    <th>&nbsp;</th>
                                        
                                    @endunless
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @for ($i = 0; $i < 3; $i++) --}}
                                    <tr>
                                        <td>1</td>
                                        <td>Hotel Ibis</td>
                                        <td>Jakarta</td>
                                        
                                        <td>Rp1.500.000</td>
                                        @unless ($past)
                                        <td>
                                            <a href="#" class="btn btn-sm btn-warning">Edit</a>
                                        </td>
                                        @endunless
                                        
                                    </tr>
                                {{-- @endfor --}}
                            </tbody>
                        </table>
                        <div class="card-footer border-secondary">
                            <i class="fad fa-fw fa-sack"></i> <b style="" class="text-danger float-right">Rp1.500.000</b>
                            
                        </div>
                    </div>

                    @unless ($past)
                    <div class="card mb-2">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                        <h2 class="mb-0 mt-1">Rp3.900.000</h2>

                                </div>
                                <div class="col-md-4">
                                        <a href="{{route('payments.method', $trip->id)}}" class="btn btn-lg btn-danger float-right">Bayar</a>

                                </div>
                            </div>
                        </div>
                    </div>
                    @endunless

                    

                </div>
                <div class="col-md-4">
                    @include('trips.operator')
                </div>
            </div>
        </div>
    </div>

    
@endsection