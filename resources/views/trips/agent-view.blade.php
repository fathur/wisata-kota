@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8">
                    <div class="card mb-2">
                        <div class="card-header">
                            Wisata
                        </div>
                        <table class="table">
                            <tr>
                                <th>Nama</th>
                                <td>{{$trip->name}}</td>
                            </tr>
                            <tr>
                            <th>Deskripsi singkat</th>
                            <td>{{$trip->description}}</td>
                            </tr>
                            <tr>
                            <th>Harga</th>
                            <td>{{$trip->price}}</td>
                            </tr>
                            <tr>
                            <th>Tanggal mulai</th>
                            <td>{{$trip->start_at}}</td>
                            </tr>
                            <tr>
                            <th>Tanggal selesai</th>
                            <td>{{$trip->end_at}}</td>
                            </tr>
                            <tr>
                            <th>Kuota</th>
                            <td>{{$trip->quota}}</td>
                            </tr>
                            <tr>
                            {{-- <th>Provinsi</th>
                            <td>Wista kemana aja</td>
                            </tr>
                            <tr>
                            <th>Kabupaten</th>
                            <td>Wista kemana aja</td>
                            </tr>
                            <tr>
                            <th>Kecamatan</th>
                            <td>Wista kemana aja</td> --}}
                            </tr>
                        </table>
                        
                    </div>
                </div>

                <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <ol class="list-unstyled">
                                    <li>
                                        <i class="fas fa-users fa-fw"></i>
                                    <a href="{{route('members.index', 3)}}">
                                            10/30 Peserta
                                        </a>
                                    </li>
                                    <li>
                                        <i class="fas fa-sack fa-fw"></i>
                                        Rp30.000.000,00
                                    </li>
                                </ol>
                                
                            </div>
                        </div>
                    </div>
                
            </div>

            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Detail</div>
                        <div class="card-body">
                            <form action="">
                                <div class="form-group">
                                    <label for="detail">Detail</label>
                                    <textarea name="detail" id="detail" class="form-control"></textarea>
                                </div>

                                <a href="#" class="btn btn-primary float-right">Simpan</a>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-md-8 mt-2 mb-2">
                    <div class="card">
                        <div class="card-header">Itinerary
                            <a href="#" class="btn btn-primary float-right btn-sm">Tambah</a>
                        </div>

                        <div class="card-body">
                                <h4>Senin, 10 Oktober 2011</h4>
                                <table class="table table-sm table-hover">
                                    <tr>
                                        <th>Waktu</th>
                                        <th>Kegiatan</th>
                                    </tr>

                                    @for($i = 0; $i < 20; $i++)
                                    <tr>
                                        <td>07:00 - 10:00</td>
                                        <td>Makan</td>
                                    </tr>
                                    @endfor
                                </table>

                                <h4>Selasa, 11 Oktober 2011</h4>
                                <table class="table table-sm table-hover">
                                        <tr>
                                            <th>Waktu</th>
                                            <th>Kegiatan</th>
                                        </tr>

                                        @for($i = 0; $i < 10; $i++)
                                        <tr>
                                            <td>07:00 - 10:00</td>
                                            <td>Makan</td>
                                        </tr>
                                        @endfor
                                    </table>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>
    </div>

    
@endsection