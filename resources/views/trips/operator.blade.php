<div class="card border-secondary">
        <div class="card-header border-secondary">
                <div class="row">
                        <div class="col-md-12">
                            <img src="https://picsum.photos/50" alt="" class="rounded-circle float-left">
                            <h5 class="float-left mt-3 ml-2">{{$trip->agent->name}}</h5>
                        </div>
                    </div>
        </div>
        <div class="card-body">
            

            {{-- <div class="clearfix line mt-3"></div> --}}
            
            <div class="row">
                <div class="col-md-12">
                    <ol class="list-unstyled">
                        <li>
                            <i class="fad fa-fw fa-map-marker-alt"></i> Jepara, Jawa Tengah
                        </li>
                        <li>
                            <i class="fad fa-fw fa-thumbs-up"></i> 99% (3432 feedback)
                        </li>
                        <li>
                            <i class="fad fa-fw fa-car-bus"></i> 34 wisata
                        </li>
                    </ol>

                    <ol class="list-inline">
                        <li class="list-inline-item">
                            <a href="#" class="text-primary"><i class="fab fa-fw fa-facebook-square fa-2x"></i></a>
                        </li>
                        <li class="list-inline-item">
                                <a href="#" class="text-info"><i class="fab fa-fw fa-twitter-square fa-2x"></i></a>

                        </li>
                        <li class="list-inline-item">
                                <a href="#" class="text-warning"><i class="fab fa-fw fa-instagram fa-2x"></i></a>

                        </li>
                        <li class="list-inline-item">
                            <a href="#" class="text-danger"><i class="fab fa-fw fa-youtube fa-2x"></i></a>

                        </li>
                    </ol>

                </div>
            </div>

        </div>

        <div class="card-footer border-secondary">
            <a href="#" class="btn btn-info btn-block">
                <i class="fas fa-stars"></i> Favoritkan</a>
        </div>
    </div>