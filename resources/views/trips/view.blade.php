@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8">


                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="https://picsum.photos/300" alt="" class="img-fluid">
                                        </div>
                                        <div class="col-md-8">
                                            <h4>{{$trip->name}}</h4>
                                            <p>{{$trip->description}}</p>
                                            <h3><span class="text-danger">{{$trip->price}}</span><small>/orang</small></h3>

                                            <ol class="list-unstyled">
                                                <li>
                                                    <i class="fad fa-fw fa-map-marker-alt"></i> Dieng, Jawa Tengah
                                                </li>
                                                <li>
                                                    <i class="fad fa-fw fa-calendar-alt"></i> {{$trip->going_at}}
                                                </li>
                                                <li>
                                                    <i class="fad fa-fw fa-users"></i> 34/{{$trip->quota}}
                                                </li>
                                            </ol>

                                            <form action="#">
                                                <div class="form-group">
                                                    <label for="total-member">Jumlah peserta</label>
                                                    <input type="number" class="form-control" placeholder="Jumlah peserta">
                                                </div>
                                            </form>

                                            <a href="{{route('trips.booking', $trip->id)}}" class="btn btn-lg btn-block btn-success"><i class="fas fa-layer-plus"></i> Ikut</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="row mt-3 mb-3">
                        <div class="col-md-12">
                            <div class="card border-success">
                                <div class="card-header border-success">
                                    <ul class="nav nav-tabs card-header-tabs " id="trip-tabs" role="tablist">
                                    <li class="nav-item ">
                                        <a class="nav-link  active" href="#detail" aria-selected="true"><i class="fal fa-pen"></i> Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#itinerary"><i class="fal fa-clock"></i> Itinerary</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#culinary"><i class="fal fa-utensils-alt"></i> Kuliner</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#feedback"><i class="fal fa-comment-lines"></i> Feedback</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#review"><i class="fal fa-comments"></i> Ulasan</a>
                                    </li>
                                    </ul>
                                </div>
                                <div class="card-body ">
                                    <div class="tab-content">
                                        <div class="tab-pane active" role="tabpanel" id="detail">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit, metus sed pharetra gravida, nibh felis sollicitudin nibh, vitae commodo justo dolor in libero. Fusce hendrerit mauris ut nisi blandit accumsan. Curabitur feugiat sapien faucibus, commodo nisl ultrices, aliquam est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut blandit lorem at elit tempor consequat. Nam sollicitudin hendrerit risus, ut laoreet lacus gravida et. Mauris tincidunt fringilla turpis ac congue. Sed nisl diam, feugiat euismod dignissim nec, mollis nec massa. Fusce in lorem id orci ornare rhoncus ut in mauris. In tempor nunc id erat scelerisque, id vestibulum justo elementum. Nam et hendrerit felis.</p>

                                            <p>Mauris in felis eu urna ornare dictum. Donec auctor augue felis, ac mattis mauris tincidunt sed. Cras nec efficitur est. Ut pulvinar magna ut euismod suscipit. Nam cursus tincidunt faucibus. Duis massa ante, vulputate sit amet eros vitae, pretium porttitor magna. Praesent pellentesque consequat nisi quis bibendum. Morbi lacinia malesuada consequat. Donec in risus lobortis, luctus quam malesuada, bibendum lorem. Nam sit amet neque sapien.</p>
                                                    
                                            <p>Sed mattis tincidunt sollicitudin. Curabitur fermentum semper ante, cursus facilisis dolor rutrum vitae. Sed nec turpis rutrum, ullamcorper velit eu, condimentum arcu. Maecenas vitae elit vitae ex egestas interdum. Ut id sem at augue pellentesque tempus. Mauris facilisis vestibulum nibh, sit amet ultrices nunc suscipit et. Proin pharetra mattis odio, fringilla mollis leo consequat et.v</p>
                                        </div>

                                        <div class="tab-pane" role="tabpanel" id="itinerary">
                                            <h4>Senin, 10 Oktober 2011</h4>
                                            <table class="table table-sm table-hover">
                                                <tr>
                                                    <th>Waktu</th>
                                                    <th>Kegiatan</th>
                                                </tr>

                                                @for($i = 0; $i < 20; $i++)
                                                <tr>
                                                    <td>07:00 - 10:00</td>
                                                    <td>Makan</td>
                                                </tr>
                                                @endfor
                                            </table>

                                            <h4>Selasa, 11 Oktober 2011</h4>
                                            <table class="table table-sm table-hover">
                                                    <tr>
                                                        <th>Waktu</th>
                                                        <th>Kegiatan</th>
                                                    </tr>
    
                                                    @for($i = 0; $i < 10; $i++)
                                                    <tr>
                                                        <td>07:00 - 10:00</td>
                                                        <td>Makan</td>
                                                    </tr>
                                                    @endfor
                                                </table>
                                        </div> 

                                        <div class="tab-pane" role="tabpanel" id="culinary">
                                            <div class="row culinary-latest">
                                                @for ($i = 0; $i<4; $i++)    
                                    
                                                <div class="col-md-12">
                                        
                                        
                                                    <div class="culinary-item mb-3">
                                        
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <img src="https://picsum.photos/400" alt="" class="img-fluid">
                                                            </div>
                                                            <div class="col-8 description">
                                                                <a href="#">
                                                                    <h3>Title lorem</h3>
                                                                </a>
                                        
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus mauris eget libero sodales, ut egestas eros facilisis. 
                                                                    Vivamus vehicula, purus vel tempus pretium, libero nisi ultricies mauris, ut venenatis est nisi at metus. 
                                                                    Proin tincidunt maximus ornare. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
                                                                    In luctus nisl urna, in viverra velit sodales non. Ut eget ultrices nisl. </p>
                                                            </div>
                                                        </div>
                                        
                                                        
                                        
                                                    </div>
                                                </div>
                                                @endfor
                                        
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" role="tabpanel" id="feedback">
                                            <ol class="list-unstyled">
                                                @for ($i = 0; $i < 100; $i++)
                                                    
                                                <li>
                                                    <img src="https://picsum.photos/50" alt="" class="rounded-circle float-left">

                                                    <div class="ml-2 float-left">
                                                        <small>Oleh XXX, 17 Agustus 2009</small>
                                                        <p>Lorem ipsum</p>
                                                    </div>

                                                    <div class="clearfix line"></div>
                                                </li>
                                                @endfor

                                            </ol>
                                        </div>

                                        <div class="tab-pane" role="tabpanel" id="review">
                                            <ol class="list-unstyled">
                                                @for ($i = 0; $i < 100; $i++)
                                                    
                                                <li>
                                                    <img src="https://picsum.photos/51" alt="" class="rounded-circle float-left">

                                                    <div class="ml-2 float-left">
                                                        <small>Oleh XXX, 17 Agustus 2009</small>
                                                        <p>Lorem ipsum</p>
                                                    </div>

                                                    <div class="clearfix line"></div>
                                                </li>
                                                @endfor

                                            </ol>
                                        </div>
                                    </div>

                                    {{-- <h5 class="card-title">Special title treatment</h5>
                                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-4">
                    @include('trips.operator')
                </div>
            </div>
        </div>
    </div>

    
@endsection

@push('script')
    <script>
        $('#trip-tabs a').on('click', function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
    </script>
@endpush