@extends('layout.main')

@section('content')
    

    <div class="section gray">  
            

        <div class="container list-trips">
            <div class="row">
                {{-- <div class="col-md-12"> --}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Library</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                {{-- </div> --}}
            </div>
                

            <div class="row">
                <div class="col-md-8">
                   <div class="card">
                       <div class="card-header">
                           Pesanan
                       </div>

                       <table class="table">
                           <thead>
                               <tr>
                                   <th>Id</th>
                                   <th>Nama pesanan</th>
                                   <th>Tanggal</th>
                                   <th>Status</th>
                               </tr>
                           </thead>

                           <tbody>
                               <tr>
                                   <td>#9393</td>
                                   <td><a href="{{route('trips.booking', 3)}}">Gunung Merbabu, Pesawat: Jakarta-Semarang, Penginapan: Ibis Hotel</a></td>
                                   <td>10 Oktober 2011</td>
                                   <td>Terbayar</td>
                               </tr>

                               <tr>
                                    <td>#9398</td>
                                    <td>Pesawat: Solo-Jakarta</td>
                                    <td>10 Oktober 2011</td>
                                    <td>Terbayar</td>
                                </tr>
                           </tbody>
                       </table>

                       
                   </div>

                   <div class="card mt-2 mb-2">
                        <div class="card-header">
                            Riwayat
                        </div>
 
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nama pesanan</th>
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
 
                            <tbody>
                                <tr>
                                    <td>#9393</td>
                                    <td><a href="{{route('trips.booking', 3)}}?past=true">Gunung Merbabu, Pesawat: Jakarta-Semarang, Penginapan: Ibis Hotel</a></td>
                                    
                                    <td>10 Oktober 2003</td>
                                    <td>Terbayar</td>
                                </tr>
 
                                <tr>
                                     <td>#9398</td>
                                     <td>Pesawat: Solo-Jakarta</td>
                                     <td>10 Oktober 2001</td>
                                     <td>Batal</td>
                                 </tr>
                            </tbody>
                        </table>
 
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    
@endsection