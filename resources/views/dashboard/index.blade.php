@extends('layout.main')

@section('content')
    <div class="section white pt-5 pb-3">  
        @include('dashboard.home-icon')
    </div>

    <div class="section gray pt-2 pb-3">  
        @include('dashboard.trip-popular')
    </div>

    <div class="section white pt-5 pb-3">  
        @include('dashboard.culinary-latest')
    </div>

    <div class="section gray pt-5 pb-3">  
        @include('dashboard.agents')
    </div>
@endsection