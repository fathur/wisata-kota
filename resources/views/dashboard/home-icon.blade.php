
    <div class="container icon-features">

            <ol class="list-inline text-center">
                <li class="list-inline-item mr-4 ml-4">
                    <a href="{{route('trips.index')}}">
                        <i class="fad fa-fw fa-car-bus fa-5x" style="--fa-primary-color: #20bf6b; --fa-secondary-color: #20bf6b;"></i>
    
                        <div class="label">
                            Wisata
                        </div>
                    </a>
                </li>
    
                <li class="list-inline-item mr-4 ml-4">
                    <a href="#">
    
                        <i class="fad fa-fw fa-plane fa-5x" style="--fa-primary-color: #3867d6; --fa-secondary-color: #3867d6;"></i>
    
                        <div class="label">
                            Pesawat
                        </div>
                    </a>
                </li>
    
                <li class="list-inline-item mr-4 ml-4">
                    <a href="#">
                        <i class="fad fa-fw fa-train fa-5x" style="--fa-primary-color: #45aaf2; --fa-secondary-color: #45aaf2;"></i>
    
                        <div class="label">
                            Kereta
                        </div>
                    </a>
                </li>
    
                <li class="list-inline-item mr-4 ml-4">
                    <a href="#">
                        <i class="fad fa-fw fa-utensils fa-5x" style="--fa-primary-color: #eb3b5a; --fa-secondary-color: #eb3b5a;"></i>
    
                        <div class="label">
                            Kuliner
                        </div>
                    </a>
                </li>
    
                <li class="list-inline-item mr-4 ml-4">
                    <a href="#">
                        <i class="fad fa-fw fa-bed fa-5x" style="--fa-primary-color: #4b6584; --fa-secondary-color: #4b6584;"></i>
    
                        <div class="label">
                            Penginapan
                        </div>
                    </a>
                </li>
            </ol>
        </div>