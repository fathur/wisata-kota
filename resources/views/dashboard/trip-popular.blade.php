<div class="container list-trips">
   
    <div class="row">
        <div class="col-md-12">
            <h2 class="sub-heading">Wisata Terlaris</h2>
        </div>
    </div>

    <div class="row">

             
        @foreach ($trips as $trip)
            
        <div class="col-md-3">
            <div class="list-trip-item">

            <img src="https://picsum.photos/400" alt="{{$trip->name}}" class="img-fluid">

                <div class="description">

                    <a href="{{route('trips.view', $trip->id)}}"><h3>{{$trip->name}}</h3></a>
                    <p>{{$trip->price}}</p>
                </div>

            </div>
        </div>
        @endforeach

    </div>
</div>