<div class="container culinary-latest">
    <div class="row">
        <div class="col-md-12">
            <h2 class="sub-heading">Kuliner Terbaru</h2>
        </div>
    </div>

    <div class="row">
        @for ($i = 0; $i<4; $i++)    

        <div class="col-md-6">


            <div class="culinary-item">

                <div class="row">
                    <div class="col-4">
                        <img src="https://picsum.photos/400" alt="" class="img-fluid">
                    </div>
                    <div class="col-8 description">
                        <a href="#">
                            <h3>Title lorem</h3>
                        </a>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus mauris eget libero sodales, ut egestas eros facilisis. 
                            Vivamus vehicula, purus vel tempus pretium, libero nisi ultricies mauris, ut venenatis est nisi at metus. 
                            Proin tincidunt maximus ornare. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
                            In luctus nisl urna, in viverra velit sodales non. Ut eget ultrices nisl. </p>
                    </div>
                </div>

              

            </div>
        </div>
        @endfor

    </div>
</div>