<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'DashboardController@index');
Route::get('dashboard', 'DashboardController@index');

Route::get('trips', 'TripController@index')->name('trips.index');
Route::get('trips/{trip}', 'TripController@view')->name('trips.view');
Route::get('trips/{trip}/booking', 'TripController@booking')
    ->name('trips.booking')
    ->middleware('auth');

Route::get('payments/{payment_id}/method', 'PaymentController@method')->name('payments.method');
Route::get('payments/{payment_id}/detail', 'PaymentController@detail')->name('payments.detail');
Route::get('payments/{payment_id}/status', 'PaymentController@status')->name('payments.status');

Route::get('orders', 'OrderController@index')->name('orders.index');

Route::get('agents/me', 'AgentController@index')
    ->name('agents.index')
    ->middleware('auth');

Route::get('agents/create', 'AgentController@create')->name('agents.create');
Route::post('agents', 'AgentController@store')->name('agents.store');

Route::get('agents/me/trips/create', 'TripController@agentCreate')->name('agents.trips.create');
Route::post('agents/me/trips', 'TripController@agentStore')->name('agents.trips.store');
Route::get('agents/me/trips/{trip}', 'TripController@agentView')->name('agents.trips.view');

Route::get('agents/me/trips/{trip}/members', 'MemberController@index')->name('members.index');

Route::get('regions', 'RegionController@index')->name('regions.index');

Auth::routes();

